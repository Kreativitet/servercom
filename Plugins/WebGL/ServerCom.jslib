var ServerCom = {
    _GetSession: function(callback)
    {
		if (!DATAHANDLER.instance)
			new DATAHANDLER.Datahandler();
		SendMessage('ServerProxy', 'OnComplete', callback + "," + DATAHANDLER.instance._token);
	},
	_Post: function(url, params)
	{
		var request = new XMLHttpRequest();
		request.open("POST", Pointer_stringify(url), true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.setRequestHeader("Authorization", "Bearer " + DATAHANDLER.instance._token);
		request.send(Pointer_stringify(params));
	},
	_Get: function(url, params, callback)
	{
		//Create a request to with the given arguments
		var request = new XMLHttpRequest();
        try {
            request.open("POST", Pointer_stringify(url), true);
			request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			request.setRequestHeader("Authorization", "Bearer " + DATAHANDLER.instance._token);

			//Subscribe a function to the ready state change, i.e. listen for request completion
			request.onload = function() {
				if (request.status === 200) {
					SendMessage('ServerProxy', 'OnComplete', callback + "," + request.responseText);
				}
				else
					SendMessage('ServerProxy', 'OnError', callback + "," + request.statusText);
			};
			request.onerror = function () {
				SendMessage('ServerProxy', 'OnError', callback + "," + request.statusText);
            };

			//Set a request timeout and subscribe a function to the timeout event
			request.timeout = 5000;
			request.ontimeout = function () {
				SendMessage('ServerProxy', 'OnError', callback + ",Request timeout. Are you connected to the internet?");
			};

			//Send the request
            request.send(Pointer_stringify(params));
        } catch (e) {
			console.log(e.message);
			SendMessage('ServerProxy', 'OnError', callback + ", Failed to connect to the server.");
		}
	},
	_GetSettings: function () {
    	var settings = DATAHANDLER.instance.loadSettings();
    	if(settings)
		{
            var jSettings = JSON.stringify(settings);
			return this._createStringBuffer(jSettings);
        }
        else
        	return this._createStringBuffer("Could not find game settings");
	},
	_createStringBuffer: function(string) {
    	var bufferSize = lengthBytesUTF8(string) + 1;
    	var buffer = _malloc(bufferSize);
    	stringToUTF8(string, buffer, bufferSize);
    	return buffer;
	}
};

mergeInto(LibraryManager.library, ServerCom);
using UnityEngine;
using System;

public class GameSettings
{
    public bool gameSound { get; set; }
    public int gameSoundVolume { get; set; }
    public string error { get; set; }

    internal static GameSettings DefaultSettings
    {
        get
        {
            return new GameSettings
            {
                gameSound = true,
                gameSoundVolume = 100
            };
        }
    }
    
    /// <summary>
    /// Decodes a JSON-string into the specified ServerPackage-type.
    /// </summary>
    /// <param name="jsonString">The JSON string that should be decoded.</param>
    /// <returns>A ServerPackage of the specified type. If the string could not be decoded it will return a ServerPackage with the error-field set.</returns>
    internal static GameSettings Decode(string jsonString)
    {
        try
        {
            return JsonUtility.FromJson<GameSettings>(jsonString);
        }
        catch (ArgumentException e)
        {
            Debug.LogError("Failed to decode game settings. The input was: " + jsonString);
            Debug.LogException(e);

            var gs = new GameSettings();
            gs.error = jsonString;
            return gs;
        }
    }
}
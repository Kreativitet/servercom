﻿using UnityEngine;
using System;
//
namespace AFC.ServerCom
{
    /// <summary>
    /// The Exercise class contains the bare minimum of data that is contained in an exercise.
    /// </summary>
    [Serializable]
    public class Exercise
    {
        /// <summary>
        /// An enum for storing the difficulty of the exercise.
        /// </summary>
        public enum Difficulty
        {
            Easy = 0, Medium = 1, Hard = 2
        }
        // All valid public properties will be converted
        /// <summary>
        /// The difficulty of this exercise. <see cref="Difficulty"/> for the three levels.
        /// </summary>
        public Difficulty difficulty;
        /// <summary>
        /// The score that was achieved by the user in this exercise.
        /// </summary>
        public int score;
        /// <summary>
        /// The score multiplier for the exercise. Used to scale the scores of the exercises to roughly the same amount.
        /// </summary>
        public float multiplier;

        /// <summary>
        /// The time in seconds it took to complete the exercise.
        /// </summary>
        public int Time;
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace AFC.ServerCom
{
    internal class CallbackHandler
    {

		private static List<CallbackHandler> _activeCallbacks = new List<CallbackHandler>();
        private static int nextId = 0;

        private int _id;
		private Callback _func;

        public int Id
        {
            get
            {
                return _id;
            }
        }

        public CallbackHandler(Callback func)
        {
            _func = func;
            _id = nextId++;
            _activeCallbacks.Add(this);
        }

        public void Trigger(bool success, string response)
        {
			if (!success)
				Debug.Log ("Trigger: " + response);
            _activeCallbacks.Remove(this);
            _func(success, response);
        }

        public static CallbackHandler GetCallback(int id)
        {
			return _activeCallbacks.Find (cb => cb.Id == id);
        }
    }
}

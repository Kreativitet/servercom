﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace AFC.ServerCom
{
    internal class ServerProxy : MonoBehaviour
    {
		private static readonly string API_URL = (Debug.isDebugBuild ? "http://cremo-project.com/api/" : "https://www.academyforcreativity.com/api/");
        public static string GAME_NAME;

        private static ServerProxy _instance;

        internal static string GetEndpoint(GameEndpoint game)
        {
            switch (game)
            {
                case GameEndpoint.CrazyConnections:
                    return "breakroom";
                case GameEndpoint.CueUp:
                    return "burstingwithideas";
                case GameEndpoint.DirectorsCut:
                    return "picory";
                case GameEndpoint.DrawInOneStroke:
                    return "drawinonestroke";
                case GameEndpoint.FigureItOut:
                    return "unexpectedfigures";
                case GameEndpoint.LanguageLab:
                    return "creativedictionary";
                case GameEndpoint.PosterPerfect:
                    return "finishthedrawing";
                case GameEndpoint.RaceForTheRaise:
                    return "boba";
                case GameEndpoint.SoundsLikeAnIdea:
                    return "weirdsounds";
                case GameEndpoint.TrendSpotter:
                    return "wordblender";
                default:
                    throw new ArgumentException("Game not found. Please use the GameEndpoint enum to specify the game.");
            }
        }

        public static ServerProxy instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType(typeof(ServerProxy)) as ServerProxy;
                    if (!_instance)
                    {
                        GameObject go = new GameObject("ServerProxy");
                        _instance = go.AddComponent<ServerProxy>();
                    }
                }
                return _instance;
            }
        }

#if UNITY_WEBGL && !UNITY_EDITOR

	[DllImport("__Internal")]
	private static extern void _Post(string url, string str);

	[DllImport("__Internal")]
	private static extern void _Get(string url, string str, int callback);

	[DllImport("__Internal")]
	private static extern void _GetSession(int callback);
    
    [DllImport("__Internal")]
    private static extern string _GetSettings();

	public void OnComplete(string str){
		triggerCallback(true, str);
	}
	
	public void OnError(string str){
		Debug.Log("OnError: " + str);
		//Split the string - we need to format the error
		var split = str.Split(',');
		if(string.IsNullOrEmpty(split[1])) {
			split[1] = "An unexpected error occured. Please check your internet connection.";
		}
		triggerCallback(false, split[0] + ',' + ServerCom.FormatError(split[1]));
	}

	private void triggerCallback(bool success, string str){
		int index = str.IndexOf(',');
		int id = int.Parse(str.Substring(0,index));
		
		CallbackHandler callback = CallbackHandler.GetCallback(id);
		if (callback != null)
			callback.Trigger(success, str.Substring(index+1));
	}
	
	public static void Post(string dir, Dictionary<string, string> dictionary = null){
        if(dictionary != null)
		    _Post(API_URL + dir, dictionaryToString(dictionary));
        else
            _Post(API_URL + dir, "");
	}

	public static void Get(string dir, Dictionary<string, string> dictionary, Callback func){
        if (instance != null)
    		_Get(API_URL + dir, dictionaryToString(dictionary), new CallbackHandler(func).Id);
        else
				func(false, ServerCom.FormatError("Please wait while the server processes your request."));
	}

	public static void Get(string dir, Callback func){
        if (instance != null)
    		_Get(API_URL + dir, "", new CallbackHandler(func).Id);
        else
				func(false, ServerCom.FormatError("Please wait while the server processes your request."));
	}

    public static void GetFromGameAPI(string dir, Dictionary<string, string> args, Callback func) {
        if(instance != null)
            _Get(API_URL + GAME_NAME + '/' + dir, dictionaryToString(args), new CallbackHandler(func).Id);
        else
				func(false, ServerCom.FormatError("Please wait while the server processes your request."));
    }
    public static void GetFromGameAPI(string dir, Callback func) {
        if(instance != null)
            _Get(API_URL + GAME_NAME + '/' + dir, "", new CallbackHandler(func).Id);
        else
				func(false, ServerCom.FormatError("Please wait while the server processes your request."));
    }

	private static string dictionaryToString(Dictionary<string, string> dictionary){
		string str = "";
		foreach(KeyValuePair<string,string> entry in dictionary)
			str += (str.Length > 0 ? "&" : "") + WWW.EscapeURL(entry.Key) + "=" + WWW.EscapeURL(entry.Value);
		return str;
	}
	
	public static void GetSession(Callback func){
        if (instance != null)
    		_GetSession(new CallbackHandler(func).Id);
        else
				func(false, ServerCom.FormatError("Failed to create a ServerProxy instance."));
	}
    
    public static GameSettings GetSettings() {
        var jSettings = _GetSettings();
        return GameSettings.Decode(jSettings);
    }

#else

        public static void Post(string dir, Dictionary<string, string> dictionary = null)
        {
            if (dictionary != null)
                instance.postToServer(API_URL + GAME_NAME + '/' + dir, dictionaryToForm(dictionary));
            else
                instance.postToServer(API_URL + GAME_NAME + '/' + dir, null);
        }

        public static void Get(string dir, Dictionary<string, string> dictionary, Callback func)
        {
            instance.StartCoroutine(instance.postToServer(API_URL + dir, dictionaryToForm(dictionary), func));
        }

        public static void Get(string dir, Callback func)
        {
            instance.StartCoroutine(instance.postToServer(API_URL + dir, null, func));
        }

        public static void GetFromGameAPI(string dir, Dictionary<string, string> args, Callback func)
        {
            Get(GAME_NAME + '/' + dir, args, func);
        }
        public static void GetFromGameAPI(string dir, Callback func)
        {
            Get(GAME_NAME + '/' + dir, func);
        }

        private static WWWForm dictionaryToForm(Dictionary<string, string> dictionary)
        {
            WWWForm form = new WWWForm();

            foreach (KeyValuePair<string, string> entry in dictionary)
                form.AddField(entry.Key, entry.Value);

            return form;
        }

        WWW buildRequest(string endpoint, WWWForm form = null)
        {
            var headers = new Dictionary<string, string>
            {
                {"Authorization", "Bearer " + ServerCom.instance.token}
            };
            var formData = form != null ? form.data : null;
            var www = new WWW(endpoint, formData, headers);
            return www;
        }

        void postToServer(string url, WWWForm form)
        {
            buildRequest(url, form);
        }

        IEnumerator postToServer(string url, WWWForm form, Callback func)
        {
            var w = buildRequest(url, form);
            yield return w;
            var b = string.IsNullOrEmpty(w.error);

            func(b, b ? w.text : ServerCom.FormatError(w.error));
        }

        public static ServerPackage GetSession(Callback func = null)
        {
            return new ServerPackage();
        }

        public static GameSettings GetSettings()
        {
            return GameSettings.DefaultSettings;
        }

#endif
    }
}

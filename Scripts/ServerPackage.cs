﻿using UnityEngine;
using System;

namespace AFC.ServerCom
{
    /// <summary>
    /// The ServerPackage class contains the bare minimum of data that is passed with each response from the server.
    /// </summary>
    [Serializable]
    public class ServerPackage
    {
        /// <summary>
        /// A flag indicating whether the request was successful or not.
        /// </summary>
        public bool success;
        /// <summary>
        /// The current user's token.
        /// </summary>
        public string token;
        /// <summary>
        /// A string containing an error message in case the request was faulty.
        /// </summary>
        public string error;
        /// <summary>
        /// The difficulty!?
        /// </summary>
        [Obsolete("Please user the difficulty field in the Exercise-class instead.")]
        public int difficulty;
        /// <summary>
        /// The number of exercises the user has completed in the game.
        /// </summary>
        public int[] exerciseCount;
        /// <summary>
        /// The time in seconds the user has played today.
        /// </summary>
        public int TimeToday;
        /// <summary>
        /// Part of the link to the next game in line.
        /// <remarks>DO NOT USE THIS AS THIS IS ONLY PARTIAL. Use NextGameUrl instead.</remarks>
        /// </summary>
        public string NextGame;
        /// <summary>
        /// The URL of the next game in line.
        /// <remarks>Note: This is the full url, whereas NextGame only holds the part after '.com/'</remarks>
        /// </summary>
        public string NextGameUrl
        {
            get
            {
                return (Debug.isDebugBuild ? "http://cremo-project.com/" : "https://www.academyforcreativity.com/") +
                       NextGame;
            }
        }
        /// <summary>
        /// A flag indicating whether the user is participating in an active course.
        /// </summary>
        public bool IsOnCourse;

        /// <summary>
        /// Decodes a JSON-string into the specified ServerPackage-type.
        /// </summary>
        /// <typeparam name="TPackage">The type of ServerPackage to decode the string as.</typeparam>
        /// <param name="jsonString">The JSON string that should be decoded.</param>
        /// <returns>A ServerPackage of the specified type. If the string could not be decoded it will return a ServerPackage with the error-field set.</returns>
        public static TPackage Decode<TPackage>(string jsonString)
            where TPackage : ServerPackage, new()
        {
            TPackage data = null;

            try
            {
                data = JsonUtility.FromJson<TPackage>(jsonString);
            }
            catch (ArgumentException e)
            {
                Debug.LogError("Failed to decode server package. The input was: " + jsonString);
                Debug.LogException(e);

                data = new TPackage();
                data.error = "Failed to decode the server's response.";
            }

            return data;
        }

        /// <summary>
        /// Encodes this ServerPackage to a JSON-string.
        /// </summary>
        /// <returns></returns>
        public string Encode()
        {
            return JsonUtility.ToJson(this);
        }
    }

    /// <summary>
    /// The generic ServerPackage class specifies a TExercise field along with and array of TExercises.
    /// </summary>
    /// <typeparam name="TExercise">The type of exercises that this ServerPackage should carry.</typeparam>
    public class ServerPackage<TExercise> : ServerPackage
        where TExercise : Exercise
    {
        public TExercise exercise;
		[Obsolete("This field should not be used. Please contact Tobias for an agreement on a new strategy.")]
        public TExercise[] exercises;
    }
}
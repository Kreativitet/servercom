﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace AFC.ServerCom
{
    /// <summary>
    /// The callback delegate is used by the ServerCom module to call back into the game loop when a response has been received for each request.
    /// </summary>
    /// <param name="success">A boolean flag, true if a valid response was received, false if not.</param>
    /// <param name="responseText">A string containing the data that was received from the server. This string should always be JSON formatted.</param>
    public delegate void Callback(bool success, string responseText);

    public enum GameEndpoint
    {
        CrazyConnections,
        CueUp,
        DirectorsCut,
        DrawInOneStroke,
        FigureItOut,
        LanguageLab,
        PosterPerfect,
        RaceForTheRaise,
        SoundsLikeAnIdea,
        TrendSpotter
    }

    /// <summary>
    /// The ServerCom class is the main driver of the ServerCom module. It can up- and downloaded exercises from the server.
    /// </summary>
    public class ServerCom {

#if UNITY_EDITOR
    /// <summary>
    /// Test username for the editor-user.
    /// </summary>
    private const string TEST_USERNAME = "test";
    /// <summary>
    /// Password for the editor-user.
    /// </summary>
    private const string TEST_PASSWORD = "ThisIsATest123123";

#endif
        /// <summary>
        /// A self-reference, i.e. the ServerCom is a singleton class.
        /// </summary>
        private static ServerCom _instance;
        /// <summary>
        /// A reference to the most recent callback function.
        /// </summary>
        private static Callback _callback;
        /// <summary>
        /// A flag that stores whether the ServerCom module has been initialized.
        /// </summary>
        private static bool _initialized = false;

        private string _token;
        internal string token
        {
            get { return _token; }
            set
            {
                _token = value;
            }
        }

        /// <summary>
        /// A reference to the ServerCom instance. Guranreed to not return null.
        /// </summary>
        public static ServerCom instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ServerCom();
                return _instance;
            }
        }

        /// <summary>
        /// Creates a new instance of the ServerCom class. It throws an <see cref="InvalidOperationException"/> if a ServerCom class has already been constructed.
        /// </summary>
        /// <param name="sessionPackage">An optional parameter containing the session credentials.</param>
        private ServerCom(string token = null)
        {
            if (!_initialized)
                throw new InvalidOperationException("ServerCom has not been initialized. Please call ServerCom.Initialize before calling any ServerCom methods.");

            this.token = token;
        }

        public static void Initialize(GameEndpoint game)
        {
            _initialized = true;
            ServerProxy.GAME_NAME = ServerProxy.GetEndpoint(game);
        }

#if UNITY_EDITOR
    /// <summary>
    /// Creates a session with the server.
    /// </summary>
    private static void createSession()
    {
        var form = new Dictionary<string, string>();
        form["username"] = TEST_USERNAME;
        form["password"] = TEST_PASSWORD;

        ServerProxy.Get("createsession.php", form, onSessionCreated);
    }

    /// <summary>
    /// Callback function for createSession. It stores the credentials and calls the callback.
    /// </summary>
    /// <param name="success">A boolean flag indicating if the request was successful or not.</param>
    /// <param name="responseText">A string containing the server's response text.</param>
    private static void onSessionCreated(bool success, string responseText)
    {
        var response = ServerPackage.Decode<ServerPackage>(responseText);
        if (success && response.success)
        {
            _instance.token = response.token;
        }

        if (_callback == null) return;
        var temp = _callback;
        _callback = null;
        temp(success, responseText);
    }

    /// <summary>
    /// Tests the connection to the server and attempts to validate the user's credentials.
    /// </summary>
    /// <param name="callback">The method that should be called whence the request has been completed.</param>
    public static void TestConnection(Callback callback)
    {
        if (_callback == null)
        {
            _callback = callback;

            if (instance.token != null)
            {
                ServerProxy.Get("testsession.php", onTestReturn);
            }
            else
            {
                createSession();
            }
        }
        else
            callback(false, FormatError("Cannot process more than one request at a time."));
    }

    /// <summary>
    /// Callback function for TestConnection.
    /// </summary>
    /// <param name="success">A boolean flag indicating if the request was successful.</param>
    /// <param name="responseText">The response text from the server.</param>
    private static void onTestReturn(bool success, string responseText)
    {
        if (_callback == null) return;
        var temp = _callback;
        _callback = null;
        temp(success, responseText);
    }

#elif UNITY_WEBGL
    
    public static void TestConnection(Callback callback)
    {
        Debug.Log("----------- TESTING CONNECTION -----------");
        if (_callback == null)
        {
            Debug.Log("GETTING SESSION");
            _callback = callback;
            ServerProxy.GetSession(testSession);
        }
        else
            callback(false, FormatError("Cannot process more than one request a a time."));
    }

    private static void testSession(bool success, string token)
    {
		Debug.Log("Got response: " + token);

		instance.token = token;
        if (!string.IsNullOrEmpty(instance.token))
        {
            Debug.Log("FOUND SESSION");
            Debug.Log("VALIDATING SESSION");
            ServerProxy.Get("testsession.php", onTestReturn);
        }
        else{
            Debug.Log("EMPTY SESSION");
			onTestReturn(false, FormatError("No valid user credentials was found."));
        }
    }

    private static void onTestReturn(bool success, string responseText)
    {
        var response = ServerPackage.Decode<ServerPackage>(responseText);
        if (success && response.success){
            Debug.Log("VALID SESSION");
        }
        Debug.Log("----------- TESTING COMPLETED -----------");
        if (_callback != null)
        {
            Callback temp = _callback;
            _callback = null;
            temp(success, responseText);
        }
    }


#endif
        /// <summary>
        /// Get the user's progress in the game from the server
        /// </summary>
        /// <remarks>Always remember to call ServerCom.Initialize before calling any ServerCom methods.</remarks>
        /// <param name="callback">The <see cref="Callback"/> method.</param>
        public static void GetProgress(Callback callback)
        {
            ServerProxy.GetFromGameAPI("getprogress.php", callback);
        }

        /// <summary>
        /// Posts an exercise to the server.
        /// </summary>
        /// <param name="exercise">The <see cref="Exercise"/> that should be posted to the server.</param>
        /// <param name="callback">The <see cref="Callback"/> method.</param>
        public static void PostExercise(Exercise exercise, Callback callback)
        {
            var form = new Dictionary<string, string>();

            //Not the nicest code ever, but it casts the difficulty enum to an integer.
            form["difficulty"] = ((int)exercise.difficulty).ToString();
            form["exercise"] = JsonUtility.ToJson(exercise);

            ServerProxy.GetFromGameAPI("postexercise.php", form, callback);
        }

        /// <summary>
        /// Resets the progress of the currently authenticated user.
        /// </summary>
        /// <param name="callback">A method to call when a result returns from the server.</param>
        public static void ResetProgress(Callback callback)
        {
            ServerProxy.Post("reset.php");
        }

        public static GameSettings GetSettings()
        {
            return ServerProxy.GetSettings();
        }

        /// <summary>
        /// Correctly formats an error message, such that it may be deserialized as a <see cref="ServerPackage"/>.
        /// </summary>
        /// <param name="errorMessage">The message that should be serialized.</param>
        /// <returns></returns>
        internal static string FormatError(string errorMessage)
        {
			return JsonUtility.ToJson(new ServerPackage() { success = false, error = errorMessage });
        }
    }
}
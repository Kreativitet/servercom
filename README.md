# README #

### What is this repository for? ###
This repository contains the server communication library for the games of Academy for Creativity.

### How do I get set up? ###
It is meant to be used as a Git submodule, more information on that matter follows.

### Who do I talk to? ###
Please contact tobiasmorell on Slack in case anything goes wrong. You may also write a mail to tmorel14@student.aau.dk
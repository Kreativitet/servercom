﻿using UnityEngine;
using System.Collections;

namespace AFC.ServerCom
{
#if !UNITY_WEBGL && UNITY_EDITOR
    internal class ServerComTest : MonoBehaviour
    {

        void Start()
        {
            Debug.Log("Session test started...");
            ServerCom.TestConnection(onTestReturn);
        }

        private void onExercisePosted(bool success, string responseText)
        {
            var response = ServerPackage.Decode<ServerPackage>(responseText);
            if (success && response.success)
            {
                Debug.Log("Post exercise successful");
            }
            else
            {
                Debug.Log("Post exercise failed");
            }
        }

        private void onProgressReturn(bool success, string responseText)
        {
            var response = ServerPackage.Decode<ServerPackage>(responseText);
            if (success && response.success)
            {
                Debug.Log("Get progress successful");
            }
            else
            {
                Debug.Log("Get progress failed");
            }
        }

        private void onTestReturn(bool success, string responseText)
        {
            var response = ServerPackage.Decode<ServerPackage>(responseText);
            if (success && response.success)
            {
                ServerCom.GetProgress(onProgressReturn);
                Debug.Log("Session test successful");
            }
            else
            {
                Debug.Log("Session test failed");
            }
        }
    }
#endif
}
